﻿using System;

namespace giefpass
{
	class Program
	{
		static void Main()
		{
			var pwd = Environment.GetEnvironmentVariable("FETCHREPOS_PWD");
			if (!string.IsNullOrEmpty(pwd))
			{
				Console.WriteLine(pwd);
			}
		}
	}
}
