﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fetchrepos
{
    class RepositoryInfo
    {
        public string Name { get; set; }

        public string Url { get; set; }
    }
}
