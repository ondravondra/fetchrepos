﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace fetchrepos
{
    class BitBucketRepo : IRepo
    {
        [DataContract]
        class BitbucketRepoInfo
        {
            [DataMember]
            public string owner;
            [DataMember]
            public string slug;
            [DataMember]
            public string is_private;
            [DataMember]
            public string name;
        }

        static IEnumerable<BitbucketRepoInfo> DownloadRepoList(string user, string password)
        {
            Console.WriteLine("Listing repositories ...");

            const string url = "https://api.bitbucket.org/1.0/user/repositories/";
            var request = WebRequest.Create(url);
            var authInfo = user + ":" + password;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers["Authorization"] = "Basic " + authInfo;

            var ws = request.GetResponse();
            var jsonSerializer = new DataContractJsonSerializer(typeof(BitbucketRepoInfo[]));
            var response = ws.GetResponseStream();
            if (response == null)
            {
                return null;
            }

            var repos = (BitbucketRepoInfo[])jsonSerializer.ReadObject(response);

            Console.WriteLine("{0} repositories found.", repos.Length);

            return repos;
        }

        public string CredentialsUrl { get { return "http://bitbucket.org"; } }

        public IList<RepositoryInfo> GetRepositories(string user, string password)
        {
            var repos = DownloadRepoList(user, password);
            var result = new List<RepositoryInfo>();
            if (repos == null)
            {
                return result;
            }
            foreach (var repo in repos)
            {
                result.Add(new RepositoryInfo
                {
                    Name = repo.name,
                    Url = GetRepositoryUrl(repo.name, user)
                });
            }
            return result;
        }

        private string GetRepositoryUrl(string name, string user)
        {
            return "https://" + user + "@bitbucket.org/" + user + "/" + name + ".git";
        }
    }
}
