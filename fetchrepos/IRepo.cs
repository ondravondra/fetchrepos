using System.Collections.Generic;

namespace fetchrepos
{
    internal interface IRepo
    {
        string CredentialsUrl { get; }
        IList<RepositoryInfo> GetRepositories(string user, string password);
    }
}