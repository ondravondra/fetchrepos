﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace fetchrepos
{
    class Program
    {
        static int Main(string [] args)
        {
            if (args == null || args.Length < 2)
            {
                Console.Error.WriteLine("Missing params: <bitbucket.org|xxx.visualstudio.com> <backup-path>");
                return 1;
            }

            var repoDomain = args[0];
            var backupPath = args[1];

            try {
                if (repoDomain.ToLower() == "bitbucket.org")
                {
                    Synchronize(backupPath, new BitBucketRepo());
                }
                else if (repoDomain.ToLower().EndsWith(".visualstudio.com"))
                {
                    Synchronize(backupPath, new VisualStudioRepo(repoDomain));
                }

                return 0;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.ToString());
                return 2;
            }
        }

        private static void Synchronize(string backupPath, IRepo repo)
        {
            string user;
            string password;
            if (!Authentication.PromptForPassword(repo.CredentialsUrl, out user, out password))
            {
                return;
            }

            var repos = repo.GetRepositories(user, password);

            EraseDeletedRepoDirs(backupPath, repos);

            PullRepositories(backupPath, user, password, repos);
        }

        private static void MakeDeletable(FileSystemInfo fsi)
        {
            fsi.Attributes = FileAttributes.Normal;
            var di = fsi as DirectoryInfo;
            if (di == null)
            {
                return;
            }

            foreach (var dirInfo in di.GetFileSystemInfos())
            {
                MakeDeletable(dirInfo);
            }
        }

        private static void EraseDeletedRepoDirs(string backupPath, IList<RepositoryInfo> repos)
        {
            foreach (var dir in Directory.GetDirectories(backupPath))
            {
                string dirName = Path.GetFileName(dir);
                if (dirName != null && !repos.Any(r => r.Name == dirName))
                {
                    Console.WriteLine("Deleting " + dirName);
                    MakeDeletable(new DirectoryInfo(dir));
                    Directory.Delete(dir, true);
                }
            }
        }

        private static void PullRepositories(string backupPath, string user, string password, IList<RepositoryInfo> repos)
        {
            foreach (var r in repos)
            {
                Console.WriteLine("Synchronizing " + r.Name);

                var dir = Path.Combine(backupPath, r.Name);
                // https://help.github.com/articles/duplicating-a-repository
                if (!Directory.Exists(dir))
                {
                    RunGit("clone --mirror " + r.Url, password, backupPath);
                }

                if (Directory.Exists(dir))
                {
                    RunGit("fetch -p origin", password, dir);
                }
            }
        }

        private static void RunGit(string command, string password, string directory)
        {
            var git = new Process
            {
                StartInfo = new ProcessStartInfo("cmd.exe", "/C git " + command)
                {
                    WorkingDirectory = directory,
                    UseShellExecute = false
                }
            };
// ReSharper disable AssignNullToNotNullAttribute
            var giefPass = Path.Combine(
                Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                "giefpass.exe");
// ReSharper restore AssignNullToNotNullAttribute

            git.StartInfo.EnvironmentVariables.Add("GIT_ASKPASS", giefPass);
            git.StartInfo.EnvironmentVariables.Add("FETCHREPOS_PWD", password);
            git.Start();
            git.WaitForExit();
        }
    }
}
