using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;

namespace fetchrepos
{
    class VisualStudioRepo : IRepo
    {
        private readonly string domainName;

        public VisualStudioRepo(string domainName)
        {
            this.domainName = domainName;
        }

        public string CredentialsUrl { get { return "https://" + domainName; } }

        public IList<RepositoryInfo> GetRepositories(string user, string password)
        {
            var result = new List<RepositoryInfo>();

            Console.WriteLine("Listing repositories ...");

            var url = "https://" + domainName + "/DefaultCollection/_apis/projects?api-version=1.0";
            var request = (HttpWebRequest) WebRequest.Create(url);
            request.Headers.Add("Authorization", "Basic " +
                Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(user + ":" + password)));

            var ws = request.GetResponse();
            var response = ws.GetResponseStream();
            if (response == null)
            {
                return result;
            }

            var serializer = new JsonSerializer();

            using (var sr = new StreamReader(response))
            using (var jtr = new JsonTextReader(sr))
            {
                var doc = (JObject) serializer.Deserialize(jtr);
                var items = (JArray)doc["value"];
                for (var i = 0; i < items.Count; i++)
                {
                    var name = items[i]["name"].Value<string>();
                    result.Add(new RepositoryInfo
                    {
                        Name = name,
                        Url = GetRepositoryUrl(name, user)
                    });
                }
            }

            return result;
        }

        private string GetRepositoryUrl(string name, string user)
        {
            var userName = user;
            var i = user.IndexOf('\\');
            if (i != -1)
            {
                userName = user.Substring(i + 1);
            }

            return "https://" + userName + "@" + domainName + "/DefaultCollection/_git/" + name;
        }
    }
}